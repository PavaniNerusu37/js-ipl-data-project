const fs = require("fs");
const csv = require("csv-parser");

//function which returns the object of top10 bowlers
function topBowlersIn2015() {
  try {
    //path of csv files
    const csvFilePathMatches = "../data/matches.csv";
    const csvFilePathDeliveries = "../data/deliveries.csv";

    const matchesData = [];
    const deliveriesData = [];

    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        let arrayOfIds = [];
        for (let i = 0; i < matchesData.length; i++) {
          if (matchesData[i].season == "2015") {
            arrayOfIds.push(matchesData[i].id);
          }
        }

        // console.log(arrayOfIds);
        // console.log(deliveriesData);

        fs.createReadStream(csvFilePathDeliveries)
          .pipe(csv())
          .on("data", (row) => {
            deliveriesData.push(row);
          })
          .on("end", () => {
            // console.log(deliveriesData);
            const obj = {};

            for (let i = 0; i < deliveriesData.length; i++) {
              if (arrayOfIds.includes(deliveriesData[i].match_id)) {
                if (obj[deliveriesData[i].bowler]) {
                  (obj[deliveriesData[i].bowler].run += parseInt(
                    deliveriesData[i].total_runs
                  )),
                    (obj[deliveriesData[i].bowler].balls += 1);
                } else {
                  obj[deliveriesData[i].bowler] = {
                    run: parseInt(deliveriesData[i].total_runs),
                    balls: 1,
                  };
                }
              }
            }
            const economyOfBowlers = {};

            for (const objects in obj) {
              let runs = obj[objects].run;
              let balls = obj[objects].balls;

              let economy = (runs / balls) * 6;

              economyOfBowlers[objects] = economy;
            }

            const sortedBowlers = Object.keys(economyOfBowlers).sort((a, b) => {
              // console.log(obj[a]);
              return economyOfBowlers[a] - economyOfBowlers[b];
            });

            const top10Bowlers = sortedBowlers.slice(0, 10);
            console.log(top10Bowlers);

            // //dumping the output in to .json file as json string by using stringify()
            const ouputPath = "../public/output/top10bowlersin2015.json";
            fs.writeFileSync(ouputPath, JSON.stringify(top10Bowlers, null, 2));
          });
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
topBowlersIn2015();
