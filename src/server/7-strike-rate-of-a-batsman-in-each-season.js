const fs = require("fs");
const csv = require("csv-parser");

//function which returns the object
function strikeRateOfABatsman() {
  try {
    //path of csv files
    const csvFilePathMatches = "../data/matches.csv";
    const csvFilePathDeliveries = "../data/deliveries.csv";

    const matchesData = [];
    const deliveriesData = [];

    const readMatchesStream = fs
      .createReadStream(csvFilePathMatches)
      .pipe(csv());

    readMatchesStream.on("data", (row) => {
      matchesData.push(row);
    });

    readMatchesStream.on("end", () => {
      const idsObject = {};
      for (let i = 0; i < matchesData.length; i++) {
        const curr = matchesData[i];
        if (!idsObject[curr.season]) {
          idsObject[curr.season] = [curr.id];
        } else {
          idsObject[curr.season].push(curr.id);
        }
      }

      const readDeliveriesStream = fs
        .createReadStream(csvFilePathDeliveries)
        .pipe(csv());

      readDeliveriesStream.on("data", (row) => {
        deliveriesData.push(row);
      });

      readDeliveriesStream.on("end", () => {
        //empty object created
        const object = {};

        for (const key in idsObject) {
          if (idsObject.hasOwnProperty(key)) {
            object[key] = {};

            for (let j = 0; j < deliveriesData.length; j++) {
              const obj = deliveriesData[j];
              if (idsObject[key].includes(obj.match_id)) {
                object[key][obj.batsman] = object[key][obj.batsman] || {
                  runs: 0,
                  balls: 0,
                };

                object[key][obj.batsman].runs += parseInt(obj.batsman_runs);
                object[key][obj.batsman].balls += 1;
              }
            }

            for (const batsman in object[key]) {
              if (object[key].hasOwnProperty(batsman)) {
                const { runs, balls } = object[key][batsman];
                object[key][batsman].strikeRate = (runs / balls) * 100 || 0;
              }
            }
          }
        }
        console.log(object);

        const outputPath =
          "../public/output/strikeRateOfBatsmanInEachSeason.json";
        //dumping the output in to .json file as json string by using stringify()
        fs.writeFileSync(outputPath, JSON.stringify(object, null, 2));
      });
    });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
strikeRateOfABatsman();
