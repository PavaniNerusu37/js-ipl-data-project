const fs = require("fs");
const csv = require("csv-parser");

//function returns the the who loss the toss but won the match
function theTeamWonTheMatchAndLossTheToss() {
  try {
    //matches.csv file path
    const csvFilePathMatches = "../data/matches.csv";

    const matchesData = [];

    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        const obj = {};

        for (let index = 0; index < matchesData.length; index++) {
          const tossWon = matchesData[index].toss_winner;
          const matchWon = matchesData[index].winner;

          if (tossWon === matchWon) {
            if (obj[matchWon]) {
              obj[matchWon] += 1;
            } else {
              obj[matchWon] = 1;
            }
          }
        }

        console.log(obj);

        // //dumping the output in to .json file as json string by using stringify()
        const outputPath = "../public/output/wonAndLossTossCount.json";
        fs.writeFileSync(outputPath, JSON.stringify(obj, null, 2));
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
theTeamWonTheMatchAndLossTheToss();
