//Find a player who has won the highest number of Player of the Match awards for each season
const fs = require("fs");
const csv = require("csv-parser");

//function returns the object of season : highestPlayerofthematch person
function playerOfTheMatchPerSeason() {
  try {
    //matches.csv file path
    const csvFilePathMatches = "../data/matches.csv";

    const matchesData = [];

    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        const obj = {};
        for (let index = 0; index < matchesData.length; index++) {
          const year = matchesData[index].season;
          const manOfTheMatch = matchesData[index].player_of_match;

          if (!obj[year]) {
            obj[year] = {};
          }
          if (obj[year][manOfTheMatch]) {
            obj[year][manOfTheMatch] += 1;
          } else {
            obj[year][manOfTheMatch] = 1;
          }
        }

        const highestAwardPerSeason = {};

        for (const year in obj) {
          let maxAwards = 0;
          let playerWithMostAwards = "";

          for (const player in obj[year]) {
            if (obj[year][player] > maxAwards) {
              maxAwards = obj[year][player];
              playerWithMostAwards = player;
            }
          }

          highestAwardPerSeason[year] = playerWithMostAwards;
        }

        console.log(highestAwardPerSeason);

        const outputPath =
          "../public/output/playerOfTheMatchForEachSeason.json";

        //dumping the output in to .json file as json string by using stringify()
        fs.writeFileSync(
          outputPath,
          JSON.stringify(highestAwardPerSeason, null, 2)
        );
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
playerOfTheMatchPerSeason();
