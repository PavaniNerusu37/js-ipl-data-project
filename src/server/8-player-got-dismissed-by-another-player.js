// Find the highest number of times one player has been dismissed by another player

const fs = require("fs");
const csv = require("csv-parser");

//function which returns the object
function playerGotDismissed() {
  try {
    //paths of csv files
    const csvFilePathDeliveries = "../data/deliveries.csv";
    const csvFilePathMatches = "../data/matches.csv";
    const deliveriesData = [];
    const matchesData = [];

    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        fs.createReadStream(csvFilePathDeliveries)
          .pipe(csv())
          .on("data", (row) => {
            deliveriesData.push(row);
          })
          .on("end", () => {
            const dismissedCounts = {};
            //iterating throught the array and finding dismissedPlayers and maxdismissing persons
            for (let i = 0; i < deliveriesData.length; i++) {
              const delivery = deliveriesData[i];
              const dismissalType = delivery.dismissal_kind;
              const dismissedPlayer = delivery.player_dismissed;
              const bowler = delivery.bowler;

              if (dismissalType !== "run out" && dismissedPlayer && bowler) {
                const key = `${dismissedPlayer}-${bowler}`;

                if (dismissedCounts[key]) {
                  dismissedCounts[key]++;
                } else {
                  dismissedCounts[key] = 1;
                }
              }
            }

            //by iterating the object we find maxDismissals,maxDismissedplayer
            let maxDismissals = 0;
            let maxDismissedPair;

            for (const key in dismissedCounts) {
              if (dismissedCounts[key] > maxDismissals) {
                maxDismissals = dismissedCounts[key];
                maxDismissedPair = key;
              }
            }

            //created object to store the deatials
            const result = {
              maxDismissed: `${maxDismissedPair.split("-")[0]}`,
              Bowler: `${maxDismissedPair.split("-")[1]}`,
              maxDismissals: maxDismissals,
            };

            //printing the result asked
            console.log(result);

            const ouputPath =
              "../public/output/playersGotDismissedByAnotherPlayers.json";

            //dumping the output in to .json file as json string by using stringify()
            fs.writeFileSync(ouputPath, JSON.stringify(result, null, 2));
          });
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
playerGotDismissed();
