//import file system module for file operations
const fs = require("fs");

//import the csv-parser module for CSV parsing
const csv = require("csv-parser");

//function which returns object of matches won per
function matchesWonPerYear() {
  try {
    //matches.csv file path
    const csvFilePathMatches = "../data/matches.csv";

    //empty arr to store parsed csv data from matches.csv
    const matchesData = [];

    // Create a readable stream for the matches
    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        //created empty object
        const winnersCount = {};

        for (let index = 0; index < matchesData.length; index++) {
          const year = matchesData[index].season;
          const teamWon = matchesData[index].winner;

          if (!winnersCount[year]) {
            winnersCount[year] = {};
          }
          if (!winnersCount[year][teamWon]) {
            winnersCount[year][teamWon] = 1;
          } else {
            winnersCount[year][teamWon]++;
          }
        }
        console.log(winnersCount);

        //dumping the output in to .json file as json string by using stringify()
        const outputPath = "../public/output/matches-won-per-year.json";
        fs.writeFileSync(outputPath, JSON.stringify(winnersCount, null, 2));
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function calling
matchesWonPerYear();
