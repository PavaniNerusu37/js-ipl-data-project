const fs = require("fs");
const csv = require("csv-parser");

//function which returns object of person and extra runs
function extraRunsIn2016() {
  try {
    //path of csv files
    const csvFilePathMatches = "../data/matches.csv";
    const csvFilePathDeliveries = "../data/deliveries.csv";

    const matchesData = [];
    const deliveriesData = [];

    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        let arrayOfIds = [];
        for (let i = 0; i < matchesData.length; i++) {
          if (matchesData[i].season == "2016") {
            arrayOfIds.push(matchesData[i].id);
          }
        }

        fs.createReadStream(csvFilePathDeliveries)
          .pipe(csv())
          .on("data", (row) => {
            deliveriesData.push(row);
          })
          .on("end", () => {
            let extraRuns = {};
            for (let i = 0; i < deliveriesData.length; i++) {
              if (arrayOfIds.includes(deliveriesData[i].match_id)) {
                if (extraRuns[deliveriesData[i].batting_team]) {
                  extraRuns[deliveriesData[i].batting_team] += parseFloat(
                    deliveriesData[i].extra_runs
                  );
                } else {
                  extraRuns[deliveriesData[i].batting_team] = parseFloat(
                    deliveriesData[i].extra_runs
                  );
                }
              }
            }
            console.log(extraRuns);

            //dumping the output in to .json file as json string by using stringify()
            const outputPath = "../public/output/extraRunsIn2016.json";
            fs.writeFileSync(outputPath, JSON.stringify(extraRuns, null, 2));
          });
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//function call
extraRunsIn2016();
