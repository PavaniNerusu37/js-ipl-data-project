//import file system module for file operations
const fs = require("fs");

//import the csv-parser module for CSV parsing
const csv = require("csv-parser");

// function which returns the object of matchs per year
function findingMatchesPerYear() {
  try {
    //matches.csv file path
    const csvFilePathMatches = "../data/matches.csv";

    //empty arr to store parsed csv data from matches.csv
    const matchesData = [];

    // Create a readable stream for the matches
    fs.createReadStream(csvFilePathMatches)
      .pipe(csv())
      .on("data", (row) => {
        matchesData.push(row);
      })
      .on("end", () => {
        //creating empty object
        const matchesPerYear = {};

        for (let index = 0; index < matchesData.length; index++) {
          const year = matchesData[index].season;

          if (!matchesPerYear[year]) {
            matchesPerYear[year] = 1;
          } else {
            matchesPerYear[year]++;
          }
        }
        console.log(matchesPerYear);

        //dumping the output in to .json file as json string by using stringify()
        const outputPath = "../public/output/matchesPerYear.json";

        fs.writeFileSync(outputPath, JSON.stringify(matchesPerYear, null, 2));
      });
  } catch (error) {
    console.log("Error occured", error);
  }
}

//calling the function
findingMatchesPerYear();
